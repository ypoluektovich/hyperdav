extern crate hyperdav;
extern crate uuid;

use hyperdav::{Client, ClientBuilder, Depth};
use uuid::Uuid;

const OWNCLOUD_URL: &'static str = "https://demo.owncloud.org/remote.php/webdav/";

fn get_client() -> Client {
    let uuid = Uuid::new_v4();
    let url = format!("{}{}", OWNCLOUD_URL, uuid);
    ClientBuilder::default()
        .credentials("test", "test")
        .build(&url)
        .unwrap()
}

#[test]
fn put_from_reader() {
    let client = get_client();
    let f = std::io::empty();
    let put_res = client.put_from_reader(f, &[""]);

    assert!(put_res.is_ok());
}

#[test]
fn put() {
    let client = get_client();
    let put_res = client.put(b"hello".as_ref(), &[""]);

    assert!(put_res.is_ok());
}

#[test]
fn get() {
    let client = get_client();
    let f = std::io::empty();
    let put_res = client.put_from_reader(f, &[""]);
    let get_res = client.get(&[""]);

    assert!(put_res.is_ok() && get_res.is_ok());
}

#[test]
fn mkcol() {
    let client = get_client();
    let mkcol_res = client.mkcol(&[""]);

    assert!(mkcol_res.is_ok());
}

#[test]
fn mv() {
    let client = get_client();
    let mkcol_res_root = client.mkcol(&[""]);
    let mkcol_res = client.mkcol(&["from"]);
    let mv_res = client.mv(&["from"], &["to"]);

    assert!(mkcol_res_root.is_ok() && mkcol_res.is_ok() && mv_res.is_ok());
}

#[test]
fn list() {
    let client = get_client();
    let mkcol_res = client.mkcol(&[""]);
    let list_res = client.list(&[""], Depth::Zero);

    assert!(mkcol_res.is_ok() && list_res.is_ok());
}
