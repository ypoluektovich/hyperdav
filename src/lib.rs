#![deny(warnings)]
#![deny(missing_docs)]
#![deny(missing_debug_implementations)]

//! # hyperdav
//! The `hyperdav` crate provides an API for interacting with the WebDAV protocol.
//!
//! It's easy to use and handles all the abstractions over HTTP for the user.
//!
//! ## GET request
//!
//! ```rust
//! # extern crate failure;
//! # extern crate hyperdav;
//! # use hyperdav::{Client};
//! # use failure::Error;
//! #
//! # fn run() -> Result<(), Error> {
//! let client = Client::new()
//!     .credentials("foo", "bar")
//!     .build("https://demo.owncloud.org/remote.php/webdav/")
//!     .unwrap();
//!
//! let mut res = client.get(&["file.txt"])?;
//! let mut buf = vec![];
//! res.copy_to(&mut buf)?;
//! # Ok(())
//! # }
//! ```
//!
//! The GET request will return a [`Response`][response] from the [`reqwest`][reqwest] crate on
//! success.
//!
//! ## PUT request
//!
//! ```rust
//! # extern crate failure;
//! # extern crate hyperdav;
//! # use hyperdav::{Client};
//! # use failure::Error;
//! #
//! # fn run() -> Result<(), Error> {
//! let client = Client::new()
//!     .credentials("foo", "bar")
//!     .build("https://demo.owncloud.org/remote.php/webdav/")
//!     .unwrap();
//! client.put(b"hello".as_ref(), &["file.txt"])?;
//!     # Ok(())
//! # }
//!
//! ```
//!
//! The PUT request will return `()` on success just to indicate it succeeded
//!
//! [response]: ./struct.Response.html
//! [reqwest]: https://crates.io/crates/reqwest

#[macro_use]
extern crate failure;
extern crate reqwest;
extern crate url;
extern crate xml;

use std::string::ToString;

pub use reqwest::Response;

pub use self::client::{Client, ClientBuilder};
pub use self::error::Error;
pub use self::response::{PropfindResponse, WellKnownProp};

mod client;
mod error;
mod response;

/// Define the depth to which we should search.
///
/// A client MUST submit a Depth header with a value of "0", "1", or
/// "infinity" with a PROPFIND request.  Servers MUST support "0" and "1"
/// depth requests on WebDAV-compliant resources and SHOULD support
/// "infinity" requests.  In practice, support for infinite-depth
/// requests MAY be disabled, due to the performance and security
/// concerns associated with this behavior.  Servers SHOULD treat a
/// request without a Depth header as if a "Depth: infinity" header was
/// included.
///
/// Source: https://tools.ietf.org/html/rfc4918#section-9.1
#[derive(Debug)]
pub enum Depth {
    /// Do not list properties of children
    Zero,
    /// List properties of direct children
    One,
    /// As deep as we can go
    Infinity,
    /// A non-standard server may accept any number in the depth header
    Number(u32),
}

impl ToString for Depth {
    fn to_string(&self) -> String {
        match *self {
            Depth::Zero => "0".to_string(),
            Depth::One => "1".to_string(),
            Depth::Infinity => "Infinity".to_string(),
            Depth::Number(depth) => depth.to_string(),
        }
    }
}
